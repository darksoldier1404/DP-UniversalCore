# DP-UniversalCore
[![Maven Central](https://img.shields.io/maven-central/v/com.darksoldier1404/DP-UniversalCore.svg?label=Maven%20Central)](https://search.maven.org/search?q=g:%22com.darksoldier1404.duc%22%20AND%20a:%22DP-UniversalCore%22)

[ Gradle ]

repositories
```
    maven {
        url "http://dpnw.site:8081/repository/maven-releases/"
        allowInsecureProtocol = true
    }
```
dependencies
```
compileOnly 'com.darksoldier1404.duc:DP-UniversalCore:1.0.3.84'
```
